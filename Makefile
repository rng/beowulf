all: beowulf

beowulf: beowulf.c
	cc -w -O2 -m32 -o $@ $< -lm

clean:
	rm -rf beowulf
