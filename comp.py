#!/usr/bin/env python

LIT, X, Y, ADD, SUB, GET, GT, LT, EQ, DUP, AND, GZ, GET2, JF, J, SHR = range(16)

p_water="""
x
x 1- y get
x 1+ y get +
x y 1- get +
x y 1+ get + 
1 >>
x y get2 -
""".replace("\n", " ")

p_life = """
x 1- y    get >0
x 1+ y    get >0 +
x    y 1+ get >0 +
x    y 1- get >0 +
x 1- y 1- get >0 +
x 1- y 1+ get >0 +
x 1+ y 1- get >0 +
x 1+ y 1+ get >0 +

x y get
if
  dup  2 = if 10
  else 3 = if 12
  else        1 1- 
  fi fi
else
  3 = if 12 else 1 1- fi
fi
""".replace("\n", " ")


def comp(code):
  asm = []
  ifstack = []
  for ins in code.split():
    try:
      v = int(ins)
      asm.extend([LIT, v])
    except:
      if ins   == "x": asm.append(X)
      elif ins == "y": asm.append(Y)
      elif ins == "1+": asm.extend([LIT, 1, ADD])
      elif ins == "1-": asm.extend([LIT, 1, SUB])
      elif ins == "get": asm.append(GET)
      elif ins == "get2": asm.append(GET2)
      elif ins == "+": asm.append(ADD)
      elif ins == "-": asm.append(SUB)
      elif ins == ">": asm.append(GT)
      elif ins == "<": asm.append(LT)
      elif ins == "=": asm.append(EQ)
      elif ins == "dup": asm.append(DUP)
      elif ins == "and": asm.append(AND)
      elif ins == ">0" : asm.append(GZ)
      elif ins == ">>": asm.append(SHR)
      elif ins == "jf": asm.append(JF)
      elif ins == "j": asm.append(J)
      elif ins == "if":
        asm.extend([JF, 0])
        ifstack.append(len(asm)-1)
      elif ins == "else":
        patch = ifstack.pop()
        asm.extend([J, 0])
        ifstack.append(len(asm)-1)
        asm[patch] = len(asm)-patch-1
      elif ins == "fi":
        patch = ifstack.pop()
        asm[patch] = len(asm)-patch-1
      else: raise Exception("unhandled op: %s" % ins)
  disasm(asm)
  return asm


def disasm(asm):
  pc = 0
  ops = "lit x y + - get > < = dup and gz get2 jf j >>".split()
  while pc < len(asm):
    print "%03d:" % pc,
    ins = asm[pc]
    if ins == LIT:
      pc += 1
      print asm[pc]
    elif ins in [JF, J]:
      pc += 1
      print ops[ins], asm[pc]
    else:
      print ops[ins]
    pc += 1


def poetry(code, textfn):
  words = open(textfn).read().replace("\n", " ").split()

  out = ""

  for ins in code:
    for i in range(ins):
      out += "%s " % words.pop(0)
    out += " "

  print "|%s|" % out

  decoded = []
  i = 0
  cnt = 0
  while i < len(out):
    while i < len(out) and (out[i] != " "): i += 1
    p = i
    while i < len(out) and (out[i] == " "): i += 1
    v = (i-p-1)
    if v == 0:
      cnt += 1
    elif v == 2:
      decoded.append(cnt+1)
      decoded.append(0)
      cnt = 0
    elif v == 1:
      decoded.append(cnt+1)
      cnt = 0
  print code
  print decoded
  assert decoded == code


def main():
  code = comp(p_life)
  print code
  poetry(code, "raw/iliad.txt")
  print

  code = comp(p_water)
  print code
  poetry(code, "raw/beowulf.txt")
  print


if __name__ == "__main__":
  main()
