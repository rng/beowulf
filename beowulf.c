/* Beowulf: Parallel poetry-powered 4bit GPU*                   rng 12/12/16 */
/* usage: cc -w -O2 -m32 -o beowulf -lm; ./beowulf [pixel shader poem]       */

/* Default pixel-shader: First two stanzas of Heaney's Beowulf translation   */
/* (Apologies for the terrible attempt at justifying with a monospace font). */

                                    char*
                                   Beowulf
                                      =
            "So.  The   Spear-Danes  in days done by  And the  "
            "kings who ruled them had  courage   and  greatness. "
            "We have  heard of  those prince's heroic campaigns. "

            "There  was Shield Sheafson,  scourge  of many   "
            "tribes,  A wrecker of mead-benches,  rampaging among "
            "foes. This terror  of the hall-troops  had  come far.   "
            "A  foundling to start  with, he would flourish later  "
            "on As his   powers  waxed and his worth was proved. "
            "In the end each clan on the outlying coats  Beyond  "
            "the whale-road  had to yield to him 10 And begin to "
            "pay tribute. That  was one good king.  ";

e[]={16,17,18,19,20,21,27,195};  // e[]={16,52,88,124,160,202,208,195}; //  red
#define o (a<0?a+w:a>=w?a-w:a)+(b<0?b+h:b>=h?b-h:b)*w             //  --+------ 
#define u(...) wprintf(__VA_ARGS__);                              //  01| li  x
#define n(a,b) for(a=0;a<b;a++)                                   //  23|  y  +
d[]={32,9617,9618,9619,9608};                                     //  45|  - @1
#define l(x) ;break; case x:                                      //  67|  >  <
#define g calloc(w*h,1)                                           //  89|  = du
#define x(m) (rand()%m)                                           //  ab| && >0
#define S(v) k[f++]=v;                                            //  cd| @2 jf
#define _ ((*e)-1) //  We cheat a bit by defining the framebuffer //  ef|  j >>
#define I b=O;a=O  // stimulus/mutator in C rather than in shader //  --+------
#define O k[--f]   //   Certainly doable in the shader, but would //   n=F(x,y,
#define W while    //    need a bigger instruction set and shader //     @1,@2)
float l=0;char *c,*q,*r,*s,*t,*m,z[200];i,j,a,b=1,p,k[200],w=100,h=40;  // size
main(int f,char **v){c=g;if(f==2) {i=fopen(v[b],"r");fseek(i,p,2);j=ftell(i)-b;
rewind(i);fread(c,j,b,i);fclose(i);} else strcpy(c, Beowulf);setlocale(p,"");u(
L"\e[?25l")srand(time(p));m=z;q=g;r=g;s=g;n(i,w*h)q[i]=x(20)>*e?_:p;W(*c) {W(*c
&&*c!=*d)c++;t=c;W(*c&&*c==*t)c++;i=c-t-b;a++;if(i&3){*m++=a;a=p;}if(i&2)*m++=p
;}W(1){u(L"\e[;H")n(j,h){n(i,w)u(L"\e[38;5;%dm%lc",e[q[i+j*w]/2],d[q[i+j*w]/4])
u(L"\n")}l+=0.01;a=(int)(cos(l*2)*_*2+cos(l*3)*_+cos(l*4)*10)%w;b=(int)(sin(l*2
)*40+sin(l*3)*_+sin(l*4)*10)%h;q[o]=_;a=(int)(cos(l)*40+cos(l*6)*5+cos(l*3)*20)
%w;b=(int)(sin(l*3)*10+sin(l*4)*20+sin(l*2)*5)%h;q[o]=_;if(x(20)>_+2){i=x(w);j=
x(h);n(a,8)q[i-1+x(3)+(j-1+x(3))*w]=_;}n(j,h){n(i,w){p=f=0;W(p<m-z) {switch(z[p
++]){l(0)S(z[p++])l(1)S(i)l(2)S(j)l(3)I;S(a+b)l(4)I;S(a-b)l(5)I;S(q[o])l(12)I;S
(s[o])l(6)I;S(a>b)l(7)I;S(a<b)l(8)I;S(a==b)l(9)a=O;S(a)S(a)l(10)I;S(a&&b)l(11)a
=O;S(a>0)l(13)a=z[p++];p+=!O?a:0;l(14)p+=z[p++];l(15)I;S(a>>b)}}a=O;r[i+j*w]=(a
>_)?_:(a<0)?0:a;}}t=s;s=q;q=r;r=t;usleep(10*d[1]);}} //*: for loose defn of GPU
